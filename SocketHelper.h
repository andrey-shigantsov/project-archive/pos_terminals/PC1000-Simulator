#ifndef SOCKETHELPER_H
#define SOCKETHELPER_H

#include <QObject>
#include <QTcpSocket>

#define POS_PC1000_SIM_EVENTLOOPS

#ifdef POS_PC1000_SIM_EVENTLOOPS
#include <QEventLoop>
#include <QTimer>
#else
#include <QElapsedTimer>
#include <QThread>
#endif

class SocketHelper : public QObject
{
  Q_OBJECT

public:
  SocketHelper(QTcpSocket * sock, QObject * parent = 0) : QObject(parent)
  {
    isWaiting = false;
    this->sock = sock;
    SleepTime = 10;
#ifdef POS_PC1000_SIM_EVENTLOOPS
    timer.setSingleShot(true);
    eventloop.connect(&timer, SIGNAL(timeout()), &eventloop, SLOT(quit()));
#endif
  }

  bool waitForConnected(int msecs = -1)
  {
#ifdef POS_PC1000_SIM_EVENTLOOPS
    eventloop.connect(sock, SIGNAL(connected()), &eventloop, SLOT(quit()));
    timer.start(msecs);
    eventloop.exec();
    eventloop.disconnect(sock, SIGNAL(connected()), &eventloop, SLOT(quit()));
#else
    timer.start();
    while(sock->state() == QAbstractSocket::ConnectingState)
    {
      if ((msecs != -1) && (timer.elapsed() >= msecs)) break;
      QThread::msleep(SleepTime);
    }
#endif
    return sock->state() == QAbstractSocket::ConnectedState;
  }

  bool waitForBytesWritten(qint64 count, int msecs)
  {
    isWaiting = true;
    WaitCount = count;
#ifdef POS_PC1000_SIM_EVENTLOOPS
    eventloop.connect(sock, SIGNAL(bytesWritten(qint64)), this, SLOT(bytesWrittenHandler(qint64)));
    timer.start(msecs);
    while(isWaiting)
    {
      if (!timer.isActive()) break;
      eventloop.exec();
    }
    eventloop.disconnect(sock, SIGNAL(bytesWritten(qint64)), this, SLOT(bytesWrittenHandler(qint64)));
#else
    connect(sock, SIGNAL(bytesWritten(qint64)), this, SLOT(bytesWrittenHandler(qint64)));
    timer.start();
    while(isWaiting)
    {
      if (timer.elapsed() >= msecs) break;
      QThread::msleep(SleepTime);
    }
#endif
    return WaitCount <= 0;
  }

  qint64 waitForBytes(int msecs)
  {
#ifdef POS_PC1000_SIM_EVENTLOOPS
    eventloop.connect(sock, SIGNAL(readyRead()), &eventloop, SLOT(quit()));
    timer.start(msecs);
    while(!sock->bytesAvailable())
    {
      if (!timer.isActive()) break;
      eventloop.exec();
    }
    eventloop.disconnect(sock, SIGNAL(readyRead()), &eventloop, SLOT(quit()));
#else
    timer.start();
    while(sock->bytesAvailable() < count)
    {
      if (timer.elapsed() >= msecs) break;
      QThread::msleep(SleepTime);
    }
#endif
    return sock->bytesAvailable();
  }

  qint64 waitForBytes(qint16 count, int msecs)
  {
#ifdef POS_PC1000_SIM_EVENTLOOPS
    eventloop.connect(sock, SIGNAL(readyRead()), &eventloop, SLOT(quit()));
    timer.start(msecs);
    while(sock->bytesAvailable() < count)
    {
      if (!timer.isActive()) break;
      eventloop.exec();
    }
    eventloop.disconnect(sock, SIGNAL(readyRead()), &eventloop, SLOT(quit()));
#else
    timer.start();
    while(sock->bytesAvailable() < count)
    {
      if (timer.elapsed() >= msecs) break;
      QThread::msleep(SleepTime);
    }
#endif
    return sock->bytesAvailable();
  }

private:
  int SleepTime;
#ifdef POS_PC1000_SIM_EVENTLOOPS
  QTimer timer;
  QEventLoop eventloop;
#else
  QElapsedTimer timer;
#endif
  QTcpSocket * sock;
  bool isWaiting;
  qint64 WaitCount;

private slots:
  void finish_wait()
  {
    isWaiting = false;
  }

  void bytesWrittenHandler(qint64 count)
  {
    WaitCount -= count;
    isWaiting = WaitCount > 0;
#ifdef POS_PC1000_SIM_EVENTLOOPS
    eventloop.quit();
#endif
  }
};

#endif // SOCKETHELPER_H
