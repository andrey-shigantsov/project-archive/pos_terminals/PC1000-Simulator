#include "api/3/wlsapi.h"
#include "../internal.h"

#include <QHostInfo>
#include <QTcpSocket>
#include "SocketHelper.h"

#include <QVector>

#define SOCKETS_COUNT 5
static QVector<QTcpSocket*> Sockets;

#define SOCKNUM_CHECK_AND_GET_SOCK(num, sock) \
do{ \
  if (Sockets.count() != SOCKETS_COUNT) \
  { \
    WARN("sockets count = %d invalid", Sockets.count()); \
    return WLS_LINKCLOSED; \
  } \
  if ((num < 1)||(num > Sockets.count())) \
  { \
    WARN("socket num = %d invalid", num); \
    return WLS_PARAMERR; \
  } \
  sock = Sockets[num-1]; \
}while(0)

#define SOCKHOST_FIND_AND_CHECK(hostvar, hostbuf) \
do{ \
  QHostInfo info = QHostInfo::fromName(QString::fromLocal8Bit((char*)hostbuf)); \
  if (info.error()) \
  { \
    WARN("get host info failure: %s", info.errorString().toLocal8Bit().data()); \
    return WLS_TCPCLOSED; \
  } \
  hostvar = info.addresses().first(); \
}while(0)

#define SOCKPORT_CONVERT_AND_CHECK(portvar, portbuf) \
do{ \
  bool ok = false; \
  portvar = QString::fromLocal8Bit((char*)portbuf).toInt(&ok); \
  if (!ok) \
  { \
    WARN("%s", "read port failure"); \
    return WLS_PARAMERR; \
  } \
}while(0)


int Wls_Init(void)
{
  return 0;
}

int Wls_Dial(const uchar *dialNum)
{
  Q_UNUSED(dialNum)

  Sockets.resize(SOCKETS_COUNT);
  for (int i = 0; i < Sockets.count(); ++i)
    Sockets[i] = new QTcpSocket();

  return 0;
}

int Wls_CheckDial(void)
{
  return Wls_CheckNetLink();
}

int Wls_NetClose(void)
{
  foreach (QTcpSocket * sock, Sockets)
    sock->deleteLater();
  Sockets.clear();

  return 0;
}

int Wls_CheckNetLink (void)
{
  if (Sockets.count() != SOCKETS_COUNT)
    return WLS_LINKCLOSED;
  return WLS_OK;
}

int Wls_Reset(void)
{
  Wls_NetClose();
  return 0;
}

//----------------------------------------------------------------------

int Wls_MTcpConnectExt(int socketNo, const uchar *destIP, const uchar *destPort, const uchar typeProtocol)
{
  Q_UNUSED(socketNo);
  Q_UNUSED(destIP)
  Q_UNUSED(destPort)
  Q_UNUSED(typeProtocol)
  EMPTY_FUNC
  return 0;
}

int Wls_MTcpPreConnectExt(int socketNo, const uchar *destIP, const uchar *destPort, const uchar typeProtocol)
{
  Q_UNUSED(socketNo);
  Q_UNUSED(destIP)
  Q_UNUSED(destPort)
  Q_UNUSED(typeProtocol)
  EMPTY_FUNC
  return 0;
}

int Wls_MTcpConnect(int socketNo, const uchar *destIP, const uchar *destPort)
{
  QTcpSocket * sock = 0;
  SOCKNUM_CHECK_AND_GET_SOCK(socketNo, sock);
  QHostAddress host;
  SOCKHOST_FIND_AND_CHECK(host, destIP);
  quint16 port;
  SOCKPORT_CONVERT_AND_CHECK(port, destPort);

  sock->connectToHost(host, port);
  SocketHelper helper(sock);
  if (!helper.waitForConnected(30000))
  {
    WARN("%s", "timeout");
    return WLS_TIMEOUT;
  }
  if (sock->state() != QAbstractSocket::ConnectedState)
  {
    WARN("failure: state = %d", sock->state());
    return WLS_TCPCLOSED;
  }
  return WLS_OK;
}

int Wls_MTcpPreConnect(int socketNo, const uchar *destIP, const uchar *destPort)
{
  Q_UNUSED(socketNo);
  Q_UNUSED(destIP)
  Q_UNUSED(destPort)
  EMPTY_FUNC
  return 0;
}

int Wls_CheckMTcpLink(int socketNo)
{
  QTcpSocket * sock = 0;
  SOCKNUM_CHECK_AND_GET_SOCK(socketNo, sock);
  switch (sock->state())
  {
  default:
    return WLS_OTHERR;
  case QAbstractSocket::UnconnectedState:
    return WLS_TCPCLOSED;
  case QAbstractSocket::HostLookupState:
  case QAbstractSocket::ConnectingState:
    return WLS_TCPOPENING;
  case QAbstractSocket::ConnectedState:
  case QAbstractSocket::BoundState:
    return WLS_OK;
  }
}

int Wls_MTcpSend(int socketNo, const uchar *sendData, ushort sendLen)
{
  return Wls_MTcpSendWithTimeOut(socketNo, sendData, sendLen, 60000);
}

int Wls_MTcpSendWithTimeOut(int socketNo, const uchar *sendData, ushort sendLen, ushort timeout)
{
  QTcpSocket * sock = 0;
  SOCKNUM_CHECK_AND_GET_SOCK(socketNo, sock);

  SocketHelper helper(sock);
  int len = sock->write((char*)sendData, sendLen);
  if (len != sendLen)
  {
    WARN("write len = %d mistmatch", len);
    return WLS_TCPSENDERR;
  }
  if (helper.waitForBytesWritten(sendLen,timeout))
    return WLS_OK;
  else
  {
    WARN("%s", "timeout");
    return WLS_TIMEOUT;
  }
}

int Wls_MTcpRecv(int socketNo, uchar *recvData, ushort *pRecvLen, ushort timeOutMs)
{
  recvData[0] = '\0';

  QTcpSocket * sock = 0;
  SOCKNUM_CHECK_AND_GET_SOCK(socketNo, sock);

  SocketHelper helper(sock);
  qint64 bufSize = *pRecvLen;
  qint64 readLen = helper.waitForBytes(timeOutMs);
  int len = qMin(bufSize, readLen);
  sock->read((char*)recvData, len);
  *pRecvLen = len;
  return WLS_OK;
}

int Wls_MTcpClose(int socketNo)
{
  QTcpSocket * sock = 0;
  SOCKNUM_CHECK_AND_GET_SOCK(socketNo, sock);
  sock->close();
  return WLS_OK;
}

//----------------------------------------------------------------------

int Wls_TcpConnectExt(const uchar *destIP, const uchar *destPort, const uchar typeProtocol)
{
  return Wls_MTcpConnectExt(1, destIP, destPort, typeProtocol);
}

int Wls_TcpPreConnectExt(const uchar *destIP, const uchar *destPort, const uchar typeProtocol)
{
  return Wls_MTcpPreConnectExt(1, destIP, destPort, typeProtocol);
}

int Wls_TcpConnect(const uchar *destIP, const uchar *destPort)
{
  return Wls_MTcpConnect(1, destIP, destPort);
}

int Wls_TcpPreConnect(const uchar *destIP, const uchar *destPort)
{
  return Wls_MTcpPreConnect(1, destIP, destPort);
}

int Wls_TcpSend(const uchar *sendData, ushort sendLen)
{
  return Wls_MTcpSend(1, sendData, sendLen);
}

int Wls_TcpSendWithTimeOut(const uchar *sendData, ushort sendLen, ushort timeout)
{
  return Wls_MTcpSendWithTimeOut(1, sendData, sendLen, timeout);
}

int Wls_TcpSendExt(const uchar *sendData, ushort sendLen)
{
  Q_UNUSED(sendData)
  Q_UNUSED(sendLen)
  EMPTY_FUNC
  return 0;
}

int Wls_TcpRecv(uchar *recvData, ushort *pRecvLen, ushort timeOutMs)
{
  return Wls_MTcpRecv(1, recvData, pRecvLen, timeOutMs);
}

int Wls_TcpClose(void)
{
  return Wls_MTcpClose(1);
}

int Wls_CheckTcpLink(void)
{
  return Wls_CheckMTcpLink(1);
}

//------------------------------------------------------------------

int Wls_SelectSim(int simno)
{
  Q_UNUSED(simno)
  EMPTY_FUNC
  return 0;
}

static BYTE level = 0x05;
int Wls_CheckSignal(int * pSignalLevel)
{
  if (level)
    --level;
  else
    level = 0x05;

  *pSignalLevel = level;
  return 0;
}

int Wls_CheckSim(void)
{
  return 0;
}

int Wls_InputSimPin(const uchar * pin)
{
  Q_UNUSED(pin)
  EMPTY_FUNC
  return 0;
}

int Wls_InputUidPwd(const uchar *uid, const uchar *pwd)
{
  Q_UNUSED(uid)
  Q_UNUSED(pwd)
  EMPTY_FUNC
  return 0;
}

int Wls_GetVerInfo(uchar *wlsType, uchar *moduleHwVer, uchar *moduleSwVer, uchar *boardVer, uchar *driverVer)
{
  Q_UNUSED(wlsType)
  Q_UNUSED(moduleHwVer)
  Q_UNUSED(moduleSwVer)
  Q_UNUSED(boardVer)
  Q_UNUSED(driverVer)
  EMPTY_FUNC
  return 0;
}

int Wls_SwitchMode(int mode)
{
  Q_UNUSED(mode)
  EMPTY_FUNC
  return 0;
}

void Wls_ResetTcpRecvBuf(void)
{
  EMPTY_FUNC
}


int Wls_SendCmd(const uchar *cmd, uchar *rsp, ushort rsplen, ushort ms)
{
  Q_UNUSED(cmd)
  Q_UNUSED(rsp)
  Q_UNUSED(rsplen)
  Q_UNUSED(ms)
  EMPTY_FUNC
  return 0;
}

int Wls_ExecuteCmd(const uchar *cmd, ushort cmdLen, uchar *rspBuf, ushort maxRspLen, ushort* outRsplen, ushort ms)
{
  Q_UNUSED(cmd)
  Q_UNUSED(cmdLen)
  Q_UNUSED(rspBuf)
  Q_UNUSED(maxRspLen)
  Q_UNUSED(outRsplen)
  Q_UNUSED(ms)
  EMPTY_FUNC
  return 0;
}

int Wls_SendCmdRequest(const uchar *cmd, ushort cmdLen)
{
  Q_UNUSED(cmd)
  Q_UNUSED(cmdLen)
  EMPTY_FUNC
  return 0;
}

int Wls_RecvCmdResponse(uchar *rspBuf, ushort maxRspLen, ushort* outRsplen, ushort ms)
{
  Q_UNUSED(rspBuf)
  Q_UNUSED(maxRspLen)
  Q_UNUSED(outRsplen)
  Q_UNUSED(ms)
  EMPTY_FUNC
  return 0;
}


int Wls_Deletemessage(unsigned char Type)
{
  Q_UNUSED(Type)
  EMPTY_FUNC
  return 0;
}

int Wls_Sendmessage(unsigned char Mode,unsigned char *PhoneNum,unsigned char *String,unsigned int Length)
{
  Q_UNUSED(Mode)
  Q_UNUSED(PhoneNum)
  Q_UNUSED(String)
  Q_UNUSED(Length)
  EMPTY_FUNC
  return 0;
}

int Wls_Readmessage(unsigned char *PhoneNum,unsigned char *SendTime,unsigned char *String,unsigned int *Length)
{
  Q_UNUSED(PhoneNum)
  Q_UNUSED(SendTime)
  Q_UNUSED(String)
  Q_UNUSED(Length)
  EMPTY_FUNC
  return 0;
}

int Wls_GetLocation(uchar mode, uchar *locationBuf, int timeoutMs)
{
  Q_UNUSED(mode)
  Q_UNUSED(locationBuf)
  Q_UNUSED(timeoutMs)
  EMPTY_FUNC
  return 0;
}

int Wls_GetIMEI(uchar *sn)
{
  sn[0] = '\0';
  EMPTY_FUNC
  return 0;
}

int Wls_GetBand(uchar *band)
{
  Q_UNUSED(band)
  EMPTY_FUNC
  return 0;
}

int Wls_SetBand(const uchar *band)
{
  Q_UNUSED(band)
  EMPTY_FUNC
  return 0;
}

int Wls_GetTime(uchar *BcdTime)
{
  Q_UNUSED(BcdTime)
  EMPTY_FUNC
  return 0;
}

int Wls_SetTime(uchar *BcdTime)
{
  Q_UNUSED(BcdTime)
  EMPTY_FUNC
  return 0;
}

int Wls_SetOperatorName(const uchar *name)
{
  Q_UNUSED(name)
  EMPTY_FUNC
  return 0;
}

int Wls_GetModuleType(uchar *moduleType)
{
  moduleType[0] = '\0';
  EMPTY_FUNC
  return 0;
}
