#ifndef JOSTYPE_H
#define JOSTYPE_H

#ifndef BYTE
#define BYTE   unsigned char
#endif
#ifndef WORD
#define WORD   unsigned short
#endif
#ifndef DWORD
#define DWORD  unsigned long
#endif
#if !defined(QT_CORE_LIB) || (QT_CORE_LIB && !defined(__cplusplus))
#ifndef uchar
#define uchar  unsigned char
#endif
#ifndef uint
#define uint   unsigned int
#endif
#ifndef ulong
#define ulong  unsigned long
#endif
#ifndef ushort
#define ushort unsigned short
#endif
#elif defined(__cplusplus)
#include<qglobal.h>
#endif

#endif // JOSTYPE_H
