#include "pos_pc1000_sim.h"
#include "mainwindow.h"

#include "josthread.h"

using namespace POS_PC1000;

Simulator::Simulator(JUserApp *userApp, QObject *parent) :
  QObject(parent)
{
  JSysThread * SysTh = new JSysThread;
  connect(SysTh, SIGNAL(finished()), this, SLOT(sysThreadFinishHandler()));
  connect(SysTh, SIGNAL(start_user_thread()), this, SLOT(startUserThread()));
  pSysTh = SysTh;
  sysTh = SysTh;

  usrApp = userApp;
  usrTh = userApp;
  if (usrTh)
    connect(usrTh, SIGNAL(finished()), this, SLOT(userThreadFinishHandler()));
  curTh = usrTh;
}

Simulator::~Simulator()
{
  delete env;
  delete sysTh;
  delete w;
  delete a;
}

int Simulator::initAppAndExec(int argc, char *argv[])
{
  a = new QApplication(argc, argv);
  connect(a, SIGNAL(aboutToQuit()), (JSysThread*)pSysTh, SLOT(stop()));
  connect(a, SIGNAL(aboutToQuit()), usrApp, SLOT(stop()));

  w = new MainWindow();

  env = new QSettings(a->applicationDirPath()+"/"+a->applicationName()+".env", QSettings::IniFormat);
  jossim_setEnv(env);

  w->show();

  startCurrentThread();
  int res = a->exec();
  stopCurrentThread();
  return res;
}

void Simulator::startSysThread()
{
  curTh = sysTh;
  startCurrentThread();
}

void Simulator::startUserThread()
{
  curTh = usrTh;
  startCurrentThread();
}

void Simulator::startCurrentThread()
{
  if (!curTh) curTh = sysTh;
  curTh->start();
}

void Simulator::stopCurrentThread()
{
  if (curTh->isRunning())
  {
    if (curTh == sysTh)
      ((JSysThread*)pSysTh)->stop();
    else if (curTh == usrTh)
      usrApp->stop();
    else
    {
      qWarning("POS_PC1000-Sim: unknown thread: terminate");
      curTh->terminate();
    }
    curTh->wait();
  }
}

void Simulator::sysThreadFinishHandler()
{
  if (curTh == sysTh)
    curTh->start();
}

void Simulator::userThreadFinishHandler()
{
  startSysThread();
}
