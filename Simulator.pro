#-------------------------------------------------
#
# Project created by QtCreator 2017-04-11T12:13:00
#
#-------------------------------------------------

QT       += core gui network
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG(debug, debug|release) {
  TARGET = POS-PC1000-SimulatorD
} else {
  TARGET = POS-PC1000-Simulator
}
TEMPLATE = lib

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES +=\
        mainwindow.cpp \
    api/3/josapi.cpp \
    api/3/joskb.cpp \
    api/3/joslcd.cpp \
    api/3/josprn.cpp \
    api/3/wlsapi.cpp \
    pos_pc1000_sim.cpp

SIMHEADERS = \
    pos_pc1000_sim.h \
    josusrapp.h \
    api/3/jostypes.h \
    api/3/josapi.h \
    api/3/wlsapi.h

HEADERS  += mainwindow.h \
    josinterfase3.h \
    $$SIMHEADERS \
    api/internal.h \
    josthread.h \
    SocketHelper.h \
    jossimEvent.h

FORMS    += mainwindow.ui

!jos_without_openssl{
  LIBS += -lcrypto
} else {
message(OPENSSL disabled)
DEFINES += JOS_WITHOUT_OPENSSL
}

POS_PC1000_SIM_DIR = $$(POS_PC1000_SDK_DIR)/TOOLS/Sim
target.path = $$POS_PC1000_SIM_DIR

simheaders.files = $$SIMHEADERS
simheaders.path = $$POS_PC1000_SIM_DIR/include

INSTALLS += target simheaders
